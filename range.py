class MyRange:
    
def my_range(start, stop, inc = 1):
    start -= inc
    while True:
        if start > stop and inc < 0:
            if (start + inc <= stop):
                break
        else:
            if (start + inc >= stop):
                break
        start += inc
        yield round(start,2)
 
    print(*my_range(5,1,-1))
